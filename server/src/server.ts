/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/

import {
	createConnection, 
	Connection, 
	ProposedFeatures,
} from "vscode-languageserver/node";

import ProjectManager from "./project/ProjectManager";

const connection:Connection = createConnection(ProposedFeatures.all);
const projectManager = new ProjectManager(connection);

projectManager.start();
