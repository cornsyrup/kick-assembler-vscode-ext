/*
	Copyright (C) 2018-2022 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/


import { spawn } from 'child_process';
import { workspace, window, Disposable, ExtensionContext, commands, Uri, WorkspaceConfiguration } from 'vscode';
import * as vscode from 'vscode';
import * as path from 'path';
import ClientUtils from '../utils/ClientUtils';
import PathUtils from '../utils/PathUtils';
import { connect } from 'http2';


export class CommandRun {

    private configuration: WorkspaceConfiguration;
    private output: vscode.OutputChannel;
    private showDiagnostics : boolean = false;

    constructor(context: ExtensionContext, output: vscode.OutputChannel) {
        this.configuration = workspace.getConfiguration('kickassembler');
        this.output = output;
        this.showDiagnostics = this.configuration.get("editor.showDiagnostics");
    }

    public runOpen(buildFilename : string = null) {

        let _file = buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(_file)) {
            _file = ClientUtils.GetOutputPath()  + path.sep + _file;
        }

        _file = path.normalize(_file);

        let _base = path.basename(_file);
        let _path = path.dirname(_file);
        
        if (buildFilename) {
            this.run(_file, _path);
        } else {
            let program = path.join(ClientUtils.GetOutputPath(), ClientUtils.CreateProgramFilename(_base));
            this.run(program, _path);
        }
        
    }

    public runStartup(buildFilename : string = null) {

        let _file = buildFilename.trim();

        /*
            when the path on the filename is not
            absolute then we want to add the
            source folder to it
        */

        if (!path.isAbsolute(_file)) {
            _file = path.dirname(ClientUtils.GetStartupUri().fsPath)  + path.sep + _file;
        }

        _file = path.normalize(_file);

        let _base = path.basename(_file);
        let _path = path.dirname(_file);
        
        if (buildFilename) {
            this.run(_file, _path, "startup");
        } else {
            let program = path.join(ClientUtils.GetOutputPath(), ClientUtils.CreateProgramFilename(_base));
            this.run(program, _path, "startup");
        }
    }

    public run(buildFilename:string, sourcePath:string, runMode:string = "open") {

        //  get emulator runtime
        let _emulator_runtime: string = this.configuration.get("emulator.runtime");

        if (!_emulator_runtime) {
            _emulator_runtime = this.configuration.get("emulatorRuntime");
        }

        let _emulator_options_str: string = this.configuration.get("emulator.options");
        if (!_emulator_options_str) {
            _emulator_options_str = this.configuration.get("emulatorOptions");
        }

        let _emulator_options: string[] = _emulator_options_str.match(/\S+/g) || [];

        // vice specific options

        let _vsf = "";
        let _use_vice_symbols: boolean = false; ;
        _use_vice_symbols = this.configuration.get("assembler.option.viceSymbols") || this.configuration.get("emulatorViceSymbols");

        if (_use_vice_symbols) {
            _vsf = buildFilename.replace(/\.(prg|d64)$/, ".vs");
        }

        // enclose in quotes to accomodate filenames with spaces on Mac

        if (process.platform == "darwin") {
            if (_emulator_runtime.search(" ") > 0) { _emulator_runtime = ClientUtils.EncloseWithQuotes(_emulator_runtime); }
            if (buildFilename.search(" ") > 0) { buildFilename = ClientUtils.EncloseWithQuotes(buildFilename); }
            if (_vsf.search(" ") > 0) { _vsf = ClientUtils.EncloseWithQuotes(_vsf); }
        }

        // handle replaceable variables

        ClientUtils.ReplaceOptionVar("${kickassembler:buildFilename}", buildFilename, _emulator_options);
        ClientUtils.ReplaceOptionVar("${kickassembler:viceSymbolsFilename}", _vsf, _emulator_options);

        // finalize options string
        
        let _options = _emulator_options.filter(function (el) {
            return el != "";
        })

        let _cwd = sourcePath;

        // show diagnostic information
        if (this.showDiagnostics) {
            this.output.show(true);
            this.output.appendLine("");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine("Run Diagnostics");
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine(`Mode             : ${runMode}`);
            this.output.appendLine(`Platform         : ${process.platform}`);
            this.output.appendLine(`Current Dir      : ${_cwd}`);
            this.output.appendLine(`Emulator Runtime : ${_emulator_runtime}`);
            this.output.appendLine(`Emulator Options :`);
            this.output.appendLine("");
            for (var i = 0; i < _options.length; i++) {
                this.output.appendLine(`  ${_options[i]}`);
            }
            this.output.appendLine("------------------------------------------------------------------------------------------------------------------------");
            this.output.appendLine("");
            }

        //  spawn child process for win32
        if (process.platform == "win32") {
            let emu = spawn(_emulator_runtime, _options, {
                detached: true,
                stdio: 'inherit',
                cwd: _cwd,
                shell: false
            });

            // console.log(emu);
            emu.unref();
            return;
        }

        //  spawn child process for osx
        if (process.platform == "darwin") {

            var _launcher:string = _emulator_runtime;

            if(_emulator_runtime.endsWith('.app')) {
                _launcher = "open";
                _options = [_emulator_runtime, "--args", ..._options];
            }

            let emu = spawn(_launcher, _options, {
                detached: true,
                stdio: 'inherit',
                cwd: _cwd,
                shell: true
            });

            emu.unref();
            return;
        }

        //  spawn child process for linux
        if (process.platform == "linux") {

            let emu = spawn(_emulator_runtime, _options, {
                detached: true,
                stdio: 'inherit',
                cwd: _cwd,
                shell: false
            });

            emu.unref();
            return;
        }

        //  create new output channel
        window.showWarningMessage(`Platform ${process.platform} is not Supported.`);
    }
}